Supplementary Materials for

> Christoph, J. & Lebert, J. (2020). Inverse Mechano-Electrical Reconstruction of Cardiac Excitation Wave Patterns from Mechanical Deformation using Deep Learning. [arXiv:2008.01640](https://arxiv.org/abs/2008.01640) [eess.IV].


### Supplementary Movie 1

![Supplementary Movie 1](Supplementary Movie 1.mov)

Reconstruction of two-dimensional chaotic electrical excitation wave dynamics from mechanical deformation in elastic excitable medium with muscle fiber anisotropy. First sequence: mechanical deformation that is analyzed by autoencoder. Second sequence: analyzed mechanical deformation (left) and reconstructed electrical excitation pattern (right). Third sequence: reconstructed electrical excitation pattern (left) and original ground truth excitation pattern (right).

### Supplementary Movie 2

![Supplementary Movie 2](Supplementary Movie 2 3D Filaments.mp4)

Reconstruction of three-dimensional electrical excitation wave dynamics from deformation in deforming bulk tissue with muscle fiber anisotropy. Left: Original electrical excitation scroll wave pattern.
Center: Reconstructed electrical excitation scroll wave pattern using autoencoder model 3Ds-A1.
Right: Vortex filaments of original and reconstructed scroll waves.

### Supplementary Movie 3

![Supplementary Movie 3](Supplementary Movie 3 3D Deformation.mp4)

Electrical scroll wave chaos and  deformation induced by the scroll wave chaos shown separately for the top layer of the bulk, c.f. Figure 7a).

### Supplementary Movie 4

![Supplementary Movie 4](Supplementary Movie 4 3D Difference.mp4)

Three-dimensional reconstruction of dataset used in [Lebert \& Christoph (2019)](https://doi.org/10.1063/1.5101041). Left: Original electrical excitation scroll wave pattern. 
Center: Reconstructed electrical excitation scroll wave pattern using autoencoder model 3Ds-A1.
Right: Difference.

### Supplementary Movie 5

![Supplementary Movie 5](Supplementary Movie 5 Temporal Sequence.mov)

Reconstruction with network model 2Dt-A3s using a temporal sequence of 3 mechanical frames to reconstruct 1 electrical frame showing estimate of electrical excitation wave pattern.

### Supplementary Movie 6

![Supplementary Movie 6](Supplementary Movie 6 Epochs.mov)

Improvement of reconstruction with increasing training duration. Reconstructed excitation spiral wave pattern after 1, 2, 4, 6, 10 and 50 training epochs and comparison with ground truth.

### Supplementary Movie 7

![Supplementary Movie 7](Supplementary Movie 7 Focal Waves.mp4)

Reconstruction of two-dimensional focal excitation wave dynamics from mechanical deformation in elastic excitable medium with muscle fiber anisotropy.

### Supplementary Movie 8

![Supplementary Movie 8](Supplementary Movie 8 3D Superresolution.mp4)

Reconstruction of three-dimensional electrical excitation wave dynamics from deformation at 4x lower spatial resolution. Left: Original electrical excitation scroll wave pattern.
Right: Reconstructed electrical excitation scroll wave pattern using autoencoder model 3Ds-D1.

### Supplementary Movie 9

![Supplementary Movie 9](Supplementary Movie 9.mp4)

Reconstruction of two-dimensional electrical excitation wave dynamics from deformation. Left: Mechanical deformation at 16×, 8× and 4× lower spatial resolutions (8×8, 16×16, 32×32 vectors) and original resolution (128×128 vectors). Note that the mesh displays 9×9, 18×18, 36×36 lines. Right: Reconstructed electrical excitation wave pattern and ground truth.

### Supplementary Movie 10
![Supplementary Movie 10](Supplementary Movie 10.mov)

Noisy mechanical displacement data (with standard deviation 0.3). The noise is Gaussian distributed and was added independently onto the $`u_x`$- and $`u_y`$-components of the displacement vectors, see also Fig. 12.
